# Whack a Mole #

Welcome to Whack a Mole! This game is written in React.js, using Redux and GraphQL
to communicate with a backend Express Server.

## Getting Set Up ##

To set up your very own client, just follow these steps:

- Update all the variable values in the `.env` file with your own values
- Run `docker-compose -f docker-compose-dev.yml up --build`

Your client is now available at `localhost:3000`, enjoy!