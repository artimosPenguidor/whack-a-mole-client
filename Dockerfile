FROM node:14.7.0

# Set working directory
WORKDIR /app/gameclient

# Copy all the files
COPY package.json .
COPY yarn.lock .

# Build server
RUN yarn

COPY . .

# Document ports needed to expose
EXPOSE 3000

ENTRYPOINT  ["/app/gameclient/entrypoint.sh"]